/*
 * CS 106B/X Sample Project
 * last updated: 2018/09/19 by Marty Stepp
 *
 * This project helps test that your Qt Creator system is installed correctly.
 * Compile and run this program to see a console and a graphical window.
 * If you see these windows, your Qt Creator is installed correctly.
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "strlib.h"
#include "console.h"
#include "simpio.h"
#include "random.h"
using namespace std;

void ClearScreen()
  {
  cout << string( 100, '\n' );
  }

//Problem 1:
int coinflip(){
    double Chance = 0.5;
    bool Result = 0;
    int heads = 0;
    int tails = 0;
    bool EndCondition = false;
    int ConsecHeads = 0;

    while (EndCondition==false) {
            Result = randomChance(Chance); /* Coinflip */
            if (Result==true){
                cout << "Heads..\n";
                heads++;
                ConsecHeads++;
            }
            else {
                cout << "Tails..\n";
                tails++;
                ConsecHeads=0;
            }
            if (ConsecHeads == 3){
                cout << "It took " << heads+tails << " throws to flip 3 heads!";
                EndCondition=true;
            }
        }
    return 0;
}

//Problem 2:
bool IsVowel(char letter){
  letter=tolower(letter);
  if (letter == 'a' | letter == 'e' | letter == 'i' | letter == 'o' | letter =='u'){
      return true;
  }
  else{
      return false;
  }
};

bool DoubleVowel(string string,int index){
    if (IsVowel(string[index]) && IsVowel(string[index+1])){
        return true;
    }
    else{
        return false;
    }
}

bool LastE(string string,int index){
    if(string[index] == 'e' && index == string.length()-1){
        return true;
    }
    else{
        return false;
    }
}


/*Language game that tranforms words by adding 'ob' before the vowels
except for
• Vowels that follow other vowels
• An e that occurs at the end of the word
*/

string obenglobish(string word){
    string trans ="";
    for (int i = 0; i < word.length(); i++){
        if (DoubleVowel(word,i)){
            trans+=word[i];
            trans+=word[i+1];
            i+=2;
        }
        if (IsVowel(word[i]) && !LastE(word,i) && !DoubleVowel(word,i)){
        trans += "ob";
        }
        trans+=word[i];
    }
    return trans;
};

// Problem 3:
//Recursive
int PascalsTriangle(int n, int k){
    if(n==k || k==0){
        return 1;
    }
    else{
        int result=PascalsTriangle(n-1,k-1)+PascalsTriangle(n-1,k);
        return result;
    }
}

void PrintTriangle(int n){
    for (int i=0; i<=n; i++){
        string print="";
        for(int j=0; j<=i; j++){
            int result = PascalsTriangle(i,j);
            print+=integerToString(result);
            print+=" ";
        }
        cout << print << "\n";
    }
}

//Problem 4:
string intToString(int integer){
    string result="";
    int remainder=integer % 10;
    int tens=integer/10;
    if (remainder == integer){
        char digit = '0'+ remainder;
        result+=digit;
    }
    else{
        result+=intToString(tens);
        char digit = '0'+ remainder;
        result+=digit;
    };
    return result;
}

int main() {
    //Problem 1:
    cout << "Want to try your luck? See how many flips it takes to get 3 consecutive heads!\n";
    string input=getLine("Press Enter to start");
    while (true) {
        coinflip();
        string word = getLine("\nPress Enter to try again or enter a q to quit): ");
        if (word == "q") break;
        ClearScreen();
    }
    ClearScreen();

    //Problem 2:
    cout << "Want to translate a word to Obenglobish?\n";
    while (true) {
        string word = getLine("Enter a word (or enter a q to quit): ");
        if (word == "q") break;
        string trans = obenglobish(word);
        cout << word << " -> " << trans << endl;
    }
    ClearScreen();

    //Problem 3:
    while (true){
    cout << "Welcome to Pascals Triangle \n";
    int n =getInteger("Enter number of rows you want to show:");
    PrintTriangle(n);
    string word = getLine("\nPress Enter to try again or enter a q to quit): ");
    if (word == "q") break;
    }
    ClearScreen();

    //Probem 4:
    int integer=getInteger("Enter integer to convert to string: \n");
    string outcome = intToString(integer);
    cout << outcome;
    cout << "\nThanks for playing!";
    return 0;
}
